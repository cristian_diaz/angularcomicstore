//carga de usuarios en caso de no existir.

if(localStorage.usuarios == undefined)
{	
	var usuarios = 
	[
		{usuario:"user" ,password:"user",nombreReal:"User Comic Globant",isAdmin:0},
		{usuario:"admin" ,password:"admin",nombreReal:"Admin Comic Globant",isAdmin:1}
	];
	
	localStorage.usuarios = JSON.stringify(usuarios);			
}

//script normal

var comicStore = angular.module('comicStore', ['ngRoute']);

comicStore.service("advertisementService",function()
{
	this.imgs=
	[
		"img/advertisements/avengers-comics-uxgzx9op.jpg",
		"img/advertisements/GalleryComics_1900x900_20140305_DTC_Cv29_52f181d89a78b0.14245228.jpg",
		"img/advertisements/green-arrow.jpg"
	];
	
	this.getUrlImg=function()
	{
		var index = (Math.ceil(Math.random() * this.imgs.length ))-1;		
		return this.imgs[index];
	}
});



comicStore.service("UsuariosService",function()
{
	this.getUsuarios = function()
	{
		if(localStorage.usuarios != undefined)
		{
			usuarios = JSON.parse(localStorage.usuarios);
		}
		else
		{	
			usuarios = [];				
		}
		
		return usuarios;
	}
	
	this.addUsuario = function (usuario)
	{
		var usuariosTemp = this.getUsuarios();
		usuariosTemp.push(usuario);
		localStorage.usuarios = JSON.stringify(usuariosTemp);
	};	
});

comicStore.service("ComicService",function()
{
	this.getComics = function()
	{
		if(localStorage.comics != undefined)
		{
			comics = JSON.parse(localStorage.comics);
		}
		else
		{
			
			comics = 
			[
				{name:"The Avengers in GEARING UP #1",img:"img/comics/portrait_incredible.jpg",isPopular:0,isAvailable:1},
				{name:"MARVEL FUTURE FIGHT #1",img:"img/comics/portrait_incredible2.jpg",isPopular:1,isAvailable:0},
				{name:"All-New, All-Different Avengers #2",img:"img/comics/portrait_incredible3.jpg",isPopular:1,isAvailable:1},
				{name:"Extraordinary X-Men #2",img:"img/comics/portrait_incredible4.jpg",isPopular:0,isAvailable:0},
				{name:"Amazing Spider-Man #648",img:"img/comics/portrait_incredible5.jpg",isPopular:0,isAvailable:1},
				{name:"Superior Spider-Man #1",img:"img/comics/portrait_incredible6.jpg",isPopular:1,isAvailable:0}
			];
			
			localStorage.comics = JSON.stringify(comics);
		}
		
		return comics;
	}
	
	this.addComic = function (comic)
	{
		var comicsTemp = this.getComics();
		comicsTemp.push(comic);
		localStorage.comics = JSON.stringify(comicsTemp);
	};	
});

comicStore.service("CharacterService",function()
{
	this.getCharacters = function()
	{
		if(localStorage.characters != undefined)
		{
			characters = JSON.parse(localStorage.characters);
		}
		else
		{			
			characters = 
			[
				{name:"X-Men",img:"img/characters/standard_xlarge.jpg",genres:["action","fiction","gang"]},
				{name:"Hulk",img:"img/characters/standard_xlarge1.jpg",genres:["super-human","nude","only-one"]},
				{name:"Captain America",img:"img/characters/standard_xlarge2.jpg",genres:["super-human","only-one"]},
				{name:"Spider-Man",img:"img/characters/standard_xlarge3.jpg",genres:["super-human","only-one"]},							
				{name:"Avengers",img:"img/characters/standard_xlarge4.jpg",genres:["action","gang"]},
				{name:"Iron Man",img:"img/characters/standard_xlarge5.jpg",genres:["tecnology","only-one"]}		
			];
			
			localStorage.characters = JSON.stringify(characters);
		}
		
		return characters;
	}
	
	this.addCharacter = function (character)
	{
		var charactersTemp = this.getCharacters();
		charactersTemp.push(character);
		localStorage.characters = JSON.stringify(charactersTemp);
	};	
});

comicStore.factory("SesionService",function(UsuariosService)
{
	var usuario = null;
	
	this.iniciarSesion = function (nombreUsuario,password)
	{
		usuario = null;
		var usuarios = UsuariosService.getUsuarios();
		
		usuarios.forEach(function(row)
		{
			if (row.usuario == nombreUsuario && row.password == password)
			{
				usuario = row;
				sessionStorage.usuario = JSON.stringify(usuario);
				return false;
			}			
		});
		
		if (this.isActive())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	this.isActive=function()
	{		
		usuario = this.getUsuario();
		
		if (usuario != null )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	this.isAdmin =function()
	{
		if (usuario.isAdmin == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	this.leave = function()
	{
		usuario = null;
		sessionStorage.clear();
		return true;
	}
	
	this.getUsuario = function()
	{
		if(usuario == null)
		{			
			if(sessionStorage.usuario != undefined )
			{
				usuario = JSON.parse(sessionStorage.usuario);				
			}
		}		
		return usuario;
	}
	
	return this;
	
});

comicStore.directive('layout1', function() {
	return {
		templateUrl: 'views/layout1.html',
		transclude: true
	};
});

comicStore.directive('layout2', function() {
	return {
		templateUrl: 'views/layout2.html',
		transclude: true
	};
});

comicStore.directive('wigetLogin', function() {
	return {
		templateUrl: 'views/widget.login.html'
	};
});

comicStore.directive('wigetComic', function() {
	return {
		templateUrl: 'views/widget.comic.html'
	};
});

comicStore.directive('wigetCharacter', function() {
	return {
		templateUrl: 'views/widget.character.html'
	};
});

comicStore.controller('widgetLoginController', function($scope,$location,UsuariosService,SesionService)
{	
	$scope.login = {usuario:"",password:"",recuerdame:false};
	$scope.registro = {usuario:"",password:"",password2:"",nombreReal:""}

	$scope.loginAction=function()
	{
		login = $scope.login;
		if(login.usuario != "" && login.password != "")
		{		
			if (SesionService.iniciarSesion(login.usuario,login.password))
			{
				$location.path('/main');
			}
			else
			{
				alert("Usuario o contraseña incorrecto");
			}			
		}
		else
		{
			alert("Debe ingresar usuario y contraseña");
		}
		
	};
	
	$scope.registerAction=function()
	{
		var validacion ="";
		var validarCEsp = /^[A-Za-z\u00C0-\u017F]*$/;
		
		registro= $scope.registro;
		
		if(registro.usuario =="")
		{
			validacion = validacion + "The user name is mandatory\n";
		}
		else
		{
			var usuarios = UsuariosService.getUsuarios();
			
			usuarios.forEach(function(row)
			{
				if(row.usuario == registro.usuario )
				{
					validacion = validacion + "The user name is already in use\n";
					return false;
				}
			});
			
			if (!validarCEsp.test(registro.usuario))
			{
				validacion = validacion + "the user name only admit a-z charateres\n";
			}
			
		}
		
		if(registro.password =="")
		{
			validacion = validacion + "The password  is mandatory\n";
		}
		else
		{
			if (!validarCEsp.test(registro.password))
			{
				validacion = validacion + "the password only admit a-z charateres\n";
			}
			
			if (registro.password.length <= 7)
			{
				validacion = validacion + "The password must have minimun 7 characteres\n";
			}
		}
		
		if(registro.password2 =="")
		{
			validacion = validacion + "The password confirmation is mandatory \n";
		}
		
		if(registro.password != registro.password2)
		{
			validacion = validacion + "The passwords aren't equal \n";
		}
		
		if(registro.nombreReal == "")
		{
			validacion = validacion + "The real name is mandatory\n";
		}
		
		if(validacion == "")
		{		
			var usuarioTemp ={usuario:registro.usuario ,password:registro.password,nombreReal:registro.nombreReal,isAdmin:0};
			UsuariosService.addUsuario(usuarioTemp);
			$("#myModal").modal('hide');
			alert("Registro Exitoso!");	
		}
		else
		{
			alert(validacion);			
		}
	}
	
});

comicStore.controller("navController",function($scope,$location,SesionService)
{
	
	$scope.isAdmin = SesionService.isAdmin();
	
	$scope.leaveAction=function()
	{
		if(SesionService.leave())
		{
			$location.path('/');
		}
	};
	
});

comicStore.controller("comicController",function($scope,ComicService)
{
	$scope.comics = ComicService.getComics();	
});

comicStore.controller("characterController",function($scope,CharacterService)
{
	$scope.characters = CharacterService.getCharacters();
});

comicStore.controller("profileController",function($scope,SesionService)
{
	$scope.usuario = SesionService.getUsuario();
});

comicStore.controller("sessionController",function($location,SesionService)
{
	//si el usuario no esta autenticado lo saca del sistema
	if (!SesionService.isActive())
	{		
		alert("No tiene una sesion activa!");
		$location.path('/');
	}
});

comicStore.controller("advertisementController",function($scope,advertisementService)
{
	$scope.urlAdvertisement = advertisementService.getUrlImg();
}); 




