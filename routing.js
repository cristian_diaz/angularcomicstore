angular.module('comicStore').config(function($routeProvider) 
{
	$routeProvider
	.when('/', {		
		templateUrl: 'views/start.html'
	})
	.when('/main', {	
		templateUrl: 'views/main.html'
	})
	.when('/profile', {	
		templateUrl: 'views/myProfile.html'
	})
	.when('/popular', {	
		templateUrl: 'views/popular.html'
	})
	.when('/advanceSearch', {
		templateUrl: 'views/advanceSearch.html'
	})
	.when('/characters', {
		templateUrl: 'views/characters.html'
	})
	.when('/account', {
		templateUrl: 'views/account.html'
	})
	.when('/404', {	
		templateUrl: 'views/404.html'
	})
	.otherwise({
		redirectTo: '/404'
	});
});




/*
angular.module('comicStore')
.config(function($locationProvider) {
$locationProvider.html5Mode(true);
});	
	*/
